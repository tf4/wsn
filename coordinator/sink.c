
/*! \file */ 
#include "contiki.h"
#include "datatypes.h"
#include "net/rime.h"
#include "dev/cc2420.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "lib/random.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "dev/serial-line.h"
#include "deluge-sink.h"
#include "sys/node-id.h"
#include "deluge.h"
#include "cfs/cfs.h"
#include "shell.h"
#include "serial-shell.h"
#include "dev/watchdog.h" //Use watchdog_reboot for soft reset

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define DEBUG 1
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define CHANNEL 135

//Header attributes regarding the application layer are added in runicast.h
/*#define APP_ATTRIBUTES        { PACKETBUF_ATTR_EPACKET_TYPE, PACKETBUF_ATTR_BIT }, \
                                MULTIHOP_ATTRIBUTES*/
			      

// Declare arrivalof command event
//static process_event_t event_command_ready;
// Declare and set initial rate of measurements
static process_event_t event_update_command;
static process_event_t event_update_done;
static process_event_t event_cts_command;
static process_event_t event_runicast_finished;
static elf_update_struct current_update;

#define MAX_RETRANSMISSIONS 0
#define NUM_HISTORY_ENTRIES 4
#define NUM_NBR_ENTRIES 12
#define ELFNAME_LEN	10
#define BASE_ELFNAME_LEN 6

/*---------------------------------------------------------------------------*/
PROCESS(command_dispatcher, "dispatcher");
PROCESS(file_transfer_process, "deluge service");
//PROCESS(dummy_process, "dummy process");
PROCESS(serial_queue, "serial input");
AUTOSTART_PROCESSES(&command_dispatcher, &file_transfer_process, &serial_queue);
/*---------------------------------------------------------------------------*/
struct nbr_entry {
  struct nbr_entry *next;
  rimeaddr_t addr;
  uint16_t rx_num;
};
LIST(nbr_table);
MEMB(nbr_mem, struct nbr_entry, NUM_NBR_ENTRIES);

struct history_entry {
  struct history_entry *next;
  rimeaddr_t addr;
  uint8_t seq;
};
LIST(history_table);
MEMB(history_mem, struct history_entry, NUM_HISTORY_ENTRIES);

/*
 * This function forwards received data from the network to the serial
 */
static void 
forward_data_to_serial(void* msgptr,unsigned int msg_size, rimeaddr_t from) {
  
  struct data_msg_struct *msg;
  int i, j;
  int sensor_num = 0;
  uint8_t mask = 0;
  ufloat fv;
  uint8_t msg_type;
  memcpy(&msg_type, msgptr, sizeof(uint8_t));
    //This message contains data of a standard query
   if((msg_type & 3) != 0 ) {
      mask = ((struct data_msg_struct*)(msgptr + offset))->sensor_mask;
      sensor_num = 0;
      //Find out which sensors were used
      for(i = 0; i < SENSOR_NUM; i++) {
	if(mask & (0x01<<i)) {
	  sensor_num++;
	}
      } 
      msg = msgptr;
      // All data in one string. Software parsing mode
	printf("#%s# %u %u %d %u %lu %u %lu.%02lu %i %u %u %u ",
	  data_prefix[msg->from & 3], from.u8[0], from.u8[1], msg->packet_seq_no,
	  packetbuf_datalen(), msg->timestamp, (msg->from >> 2),
	  (int32_t)msg->bt, ((int32_t)(msg->bt*100))%100,
	  (int)cc2420_last_rssi - 45, cc2420_last_correlation, 
	  msg->padding, mask
	);
      j = 0;
      //Using this code because printf does not support float values
      for(i = 0; i < SENSOR_NUM; i++) {
	if(mask & (0x01 << i)) {
	  memcpy(&(fv.f), (msg->values)+j, sizeof(float));
	  if(fv.f > 0) {
	    printf("%lu.%03lu ", (int32_t)fv.f,((int32_t)(fv.f*1000))%1000 );
	    //printf("%s(hex): %08lx\n",value_names[i], fv.u );
	  }
	  else {
	    fv.f = -1*fv.f;
	    printf("-%lu.%03lu ", (int32_t)fv.f,((int32_t)(fv.f*1000))%1000 );
	    //printf("%s(hex): %08lx\n",value_names[i], fv.u );
	  }
	  j += sizeof(float);
	}
      }
      if((msg->from &3) == PERIODIC_QUERY_DATA) {
	printf("%lu", *((unsigned long*)(msg->values + j)));
      }
      printf(" #!%s#\n", data_prefix[msg->from & 3]);
    }
    //This is message containing use application data
    else {
      struct app_data_msg_struct *msg;
      msg = msgptr;
      unsigned char app_values[msg->length];
      memcpy(app_values, msg->values, msg->length);
      printf("#%s# %u %u %d %u %u %i %u ",
	     data_prefix[msg->from & 3], from.u8[0], from.u8[1], msg->length,
	     packetbuf_datalen(), (msg->from >> 2), (int)cc2420_last_rssi - 45, cc2420_last_correlation);
      //The format of user application data is unknown at this point, so we're just printing the hex values
      //Data will be parsed according to a user-defined XML scheme on the Gateway
      for(i = 0; i < msg->length; i++) {
	printf("%02x", app_values[i] );
      }
      printf(" #!%s#\n", data_prefix[msg->from & 3]);
    }
  //}
}

//Not really used any more
static uint16_t update_nbr_list(const rimeaddr_t* from) {
  struct nbr_entry *ne = NULL;
  for(ne = list_head(nbr_table); ne != NULL; ne = ne->next) {
    if(rimeaddr_cmp(&ne->addr, from)) {
      ne->rx_num++;
      break;
    }
  }
  if(ne == NULL) {
    ne = memb_alloc(&nbr_mem);
    if(ne == NULL) {
      printf("New neighbor appeared, but list memory is full\n");
    }
    rimeaddr_copy(&ne->addr, from);
    ne->rx_num = 1;
    list_push(nbr_table, ne);
    printf("New neigbor %u.%u registered\n", from->u8[0], from->u8[1]);
  }
  else {
    printf("Neighbor already registered\n");
  }
  return ne->rx_num;
}

/*
 * This function executes a received command for the coordinator
 */
static void 
command_exec(struct ctrl_msg_struct* msg) {
  if(msg->cmd_type == CMD_SETRATES || msg->cmd_type == CMD_SETAPPRATES) {
    //TODO: This is of no use as the sensor service is not implemented on the sink
  }
  else if(msg->cmd_type == CMD_UPDATE){
    PRINTF("I received an update command,\n");
    //PRINTF("elf file %s is %d bytes",msg->cmd.elf_update.filename, msg->cmd.elf_update.elf_size);
    if(msg->cmd.elf_update.elf_size == 0) {
      PRINTF("to terminate deluge service on sink\n");
      process_post(&file_transfer_process, event_update_done, &(msg->cmd.elf_update));
      process_exit(&deluge_process);
    }
    else {
      //Start deluge on sink
      PRINTF("to start deluge service on sink\n");
      if(!process_is_running(&deluge_process)) {
	process_post(&file_transfer_process, event_update_command, &(msg->cmd.elf_update));
	memcpy(&current_update, &(msg->cmd.elf_update), sizeof(elf_update_struct));
      }
      else {
	printf("#UPDATE# %u.%u:BUSY:%s:%u #!UPDATE#\n", SINK_ADDR_0, SINK_ADDR_1, 
	       current_update.filename, current_update.elf_size );
      }
    }
  }
  else if(msg->cmd_type == CMD_REBOOT) {
    watchdog_reboot();
  }
}

/*
 * This function checks for valid content in a command
 * Legacy code. Checks are performed at the GW
*/
static int
is_valid_command(struct ctrl_msg_struct ctrl_msg) {
  if(ctrl_msg.cmd_type == CMD_QUERY) {
    return 1;
  }
  else if(ctrl_msg.cmd_type == CMD_UPDATE){
   if(ctrl_msg.cmd.elf_update.elf_size > 99999) {
     return 0;
   }
  }
  return 1;
} 

/*---------------------------------------------------------------------------*/
/*
 * This function is called when a network message is received.
 */
static void
recv_runicast(struct runicast_conn *c, const rimeaddr_t *from, uint8_t seqno)
{
  uint16_t rx_num;
  /* OPTIONAL: Sender history .Creates a neighbor table*/
  struct history_entry *e = NULL;
  for(e = list_head(history_table); e != NULL; e = e->next) {
    if(rimeaddr_cmp(&e->addr, from)) {
      break;
    }
  }
  if(e == NULL) {
    /* Create new history entry */
    e = memb_alloc(&history_mem);
    if(e == NULL) {
      e = list_chop(history_table); /* Remove oldest at full history */
    }
    rimeaddr_copy(&e->addr, from);
    e->seq = seqno;
    list_push(history_table, e);
  } else {
    /* Detect duplicate callback */
    if(e->seq == seqno) {
      PRINTF("runicast message received from %d.%d, seqno %d (DUPLICATE)\n",
	     from->u8[0], from->u8[1], seqno);
      PRINTF("#RXSTAT# %d %d %d DUPLICATE #!RXSTAT#\n", from->u8[0], from->u8[1], seqno);
      update_nbr_list(from);
      return;
    }
    /* Update existing history entry */
    e->seq = seqno;
  }
  //Handling content of the received message
  PRINTF("runicast message received from %d.%d, seqno %d\n",
	 from->u8[0], from->u8[1], seqno);
  PRINTF("#RXSTAT# %d %d %d UNIQUE #!RXSTAT#\n", from->u8[0], from->u8[1], seqno);
  rx_num = update_nbr_list(from);
  leds_toggle(LEDS_RED);
  
  // The message contains data just extract the payload and call forward_data_to_serial
  if(packetbuf_attr(PACKETBUF_ATTR_EPACKET_TYPE) == DATA_PACKET){
    void *rcvd_msg;
    unsigned int msg_size;
    rimeaddr_t sender;
    rcvd_msg = packetbuf_dataptr();
    msg_size = packetbuf_datalen();
    rimeaddr_copy(&sender, from);
    forward_data_to_serial(rcvd_msg, msg_size, sender);
  }
  //Message contains a control/command/acked_command message
  else {
    struct ctrl_msg_struct *rcvd_msg;
    rimeaddr_t sender;
    rcvd_msg = (struct ctrl_msg_struct*)packetbuf_dataptr();
    rimeaddr_copy(&sender, from);
    //Ack messages for file transfer service
    if(rcvd_msg->cmd_type == CMD_UPDATE) {
      //File transfer service is busy on the target leaf node
      if(rcvd_msg->cmd.elf_update.status == DELUGE_BUSY) {
	printf("#UPDATE# %u.%u:BUSY->%s/%u #!UPDATE#\n", from->u8[0], from->u8[1], 
	       rcvd_msg->cmd.elf_update.filename, rcvd_msg->cmd.elf_update.elf_size);
      }
      //File transfer service was terminated succesfully at the target leaf node
      else if(rcvd_msg->cmd.elf_update.status == DELUGE_EXIT) {
	printf("#UPDATE# %u.%u:STOPPED->%s/%u #!UPDATE#\n", from->u8[0], from->u8[1], 
	       rcvd_msg->cmd.elf_update.filename, rcvd_msg->cmd.elf_update.elf_size);
      }
      //File transfer service started at the target leaf node
      else if(rcvd_msg->cmd.elf_update.status == DELUGE_STARTED) {
	printf("#UPDATE# %u.%u:STARTED->%s/%u #!UPDATE#\n",from->u8[0], from->u8[1], 
	       rcvd_msg->cmd.elf_update.filename, rcvd_msg->cmd.elf_update.elf_size);
      }
      //Application succesfully installed on the target leaf node
      else if(rcvd_msg->cmd.elf_update.status == elf_update_SUCCESS) {
	printf("#UPDATE# %u.%u:DONE->%s/%u #!UPDATE#\n", from->u8[0], from->u8[1], 
	       rcvd_msg->cmd.elf_update.filename, rcvd_msg->cmd.elf_update.elf_size);
      }
      //Application installation failed on the target leaf node. Check returned elf loader error code
      else {
	printf("#UPDATE# %u.%u:FAILED->%s/%u:ERROR->%u #!UPDATE#\n", from->u8[0], from->u8[1], 
	       rcvd_msg->cmd.elf_update.filename, rcvd_msg->cmd.elf_update.elf_size, rcvd_msg->cmd.elf_update.status);
      }
    }
    //ACK message for a set sensor sampling rates command
    else if(rcvd_msg->cmd_type == CMD_SETRATES) {
      printf("#PRD_REQ# %u %u", sender.u8[0], sender.u8[1]);
      printf(" %u %u %u %u SET_OK #!PRD_REQ#\n", rcvd_msg->cmd.sample_rates.sensor_rates[0], rcvd_msg->cmd.sample_rates.sensor_rates[1],
	rcvd_msg->cmd.sample_rates.sensor_rates[2], rcvd_msg->cmd.sample_rates.sensor_rates[3]
      );
    }
    //operational_status_info message received from a node.
    else if(rcvd_msg->cmd_type == CMD_NETSTAT) {
      //update_nbr_list(from, rcvd_msg->cmd.bitmask);
      //update_nbr_list(from, rcvd_msg->cmd.operational_status_info.version);
      printf("#NETSTAT# %u %u %u %lu %u %u %lu.%03lu %i %u %u %u #!NETSTAT#\n", 
	     sender.u8[0], sender.u8[1], rcvd_msg->cmd.operational_status_info.version,
	     rcvd_msg->cmd.operational_status_info.timestamp, rcvd_msg->cmd.operational_status_info.deluge_status,
	     rcvd_msg->cmd.operational_status_info.tx_power,
	     (int32_t)rcvd_msg->cmd.operational_status_info.battery,((int32_t)(rcvd_msg->cmd.operational_status_info.battery*1000))%1000,
	     (int)cc2420_last_rssi - 45, cc2420_last_correlation, rcvd_msg->cmd.operational_status_info.tx_packets, rx_num
	    );
    }
    //Ack reply for a configuration command received
    else if(rcvd_msg->cmd_type == CMD_CONFIG) {
      printf("#NODECONF# %u %u %u %u %u %u OK #!NODECONF#\n", sender.u8[0], sender.u8[1],
	    rcvd_msg->cmd.node_config_cmd.bitmask, rcvd_msg->cmd.node_config_cmd.tx_power, rcvd_msg->cmd.node_config_cmd.status_info_freq, rcvd_msg->cmd.node_config_cmd.piggy_bag
      );
    }
    //This is an ACK reply for an application command
    else if(rcvd_msg->cmd_type == CMD_APPCMD) {
      printf("#APPCMD# %u %u %u #!APPCMD#\n", sender.u8[0], sender.u8[1], rcvd_msg->cmd.bitmask);
    }
  }
}

static void
sent_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
  PRINTF("runicast message sent to %d.%d, retransmissions %d\n",
	 to->u8[0], to->u8[1], retransmissions);
  //Notify serial queue that the dispatcher has become available
  process_post(&serial_queue, event_runicast_finished, NULL);
}

static void
timedout_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
  PRINTF("runicast message timed out when sending to %d.%d, retransmissions %d\n",
	 to->u8[0], to->u8[1], retransmissions);
  //Notify serial queue that the dispatcher has become available
  process_post(&serial_queue, event_runicast_finished, NULL);
}

static const struct runicast_callbacks runicast_callbacks = {recv_runicast,
							     sent_runicast,
							     timedout_runicast};
static struct runicast_conn runicast;
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(serial_queue, ev, data)
{
  //Some tests need to be done to see whether a queue is necessary
  PROCESS_BEGIN();
  PROCESS_END();
}


// Process for receiving input from serial port
PROCESS_THREAD(command_dispatcher, ev, data)
{
  static struct ctrl_msg_struct* msg;		//Send with this one
  static rimeaddr_t to;
  //static struct queuebuf* qbuf;
  PROCESS_EXITHANDLER(runicast_close(&runicast);)
    
  PROCESS_BEGIN();   
  /* Open a runicast connection on Rime channel CHANNEL. */
  runicast_open(&runicast, CHANNEL, &runicast_callbacks);
  //queuebuf_init();

  SENSORS_ACTIVATE(button_sensor);
  serial_line_init();

  serial_shell_init();
  shell_file_init();
  shell_coffee_init();
  shell_text_init();
  //shell_exec_init();
  shell_base64_init();
  shell_app_init();

  event_update_command = process_alloc_event();
  event_runicast_finished = process_alloc_event();
  event_update_done = process_alloc_event();
 
   while(1) {
     PROCESS_YIELD();
     // Wait for incoming commands from serial queue
     if(ev == PROCESS_EVENT_COMMAND) {
         if(is_valid_command(((struct cmd_packet*)data)->ctrl_msg)) {
           packetbuf_clear();
           msg = (struct ctrl_msg_struct*)packetbuf_dataptr();
           *msg = ((struct cmd_packet*)data)->ctrl_msg;
	   // Check if the command is for the coorinator node and just execute it locally
	   if(rimeaddr_cmp(&((struct cmd_packet*)data)->addr, &rimeaddr_node_addr)) {
	     command_exec(msg);
	   }
	   //Else transmit the command to its network destination
	   else {
	     if(msg->cmd_type == CMD_UPDATE) {
               if(msg->cmd.elf_update.elf_size > 0) {
		 if(process_is_running(&deluge_process)) {
		   if(strncmp(current_update.filename, msg->cmd.elf_update.filename, BASE_ELFNAME_LEN) != 0 && 
		     (current_update.elf_size != msg->cmd.elf_update.elf_size)) {
		      printf("#UPDATE# %u.%u:NOMATCH #!UPDATE#\n", ((struct cmd_packet*)data)->addr.u8[0],
			     ((struct cmd_packet*)data)->addr.u8[1] );
		      continue;
		   }
		 } //else it just exits the if and continues to send the command to the remote mode
               }
	      }
	     
	     rimeaddr_copy(&to, &((struct cmd_packet*)data)->addr);
	     
             packetbuf_set_datalen(sizeof(struct ctrl_msg_struct));
             packetbuf_set_attr(PACKETBUF_ATTR_EPACKET_TYPE, CONTROL_PACKET);
	     runicast_send(&runicast, &to, MAX_RETRANSMISSIONS);
	   }

         }
     }
   }
   PROCESS_END();
 }


#ifndef FILE_SIZE
#define FILE_SIZE 32
#endif

PROCESS_THREAD(file_transfer_process, ev, data) {

  static elf_update_struct* elf_update;
  static int fd = -1;
  static int r;
  static char file[BASE_ELFNAME_LEN];
  static int elf_size; 
  static int res = -1;
  static char buf[FILE_SIZE];
  static char bufchar[1];
  static struct etimer et;
  static int i;

  PROCESS_BEGIN();
  PRINTF("Deluge communication process started\n");
  memset(buf, 0, sizeof(buf));
  memset(bufchar, 0 , sizeof(bufchar));

  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(ev == event_update_command);
    update = (elf_update_struct*)data;
    strncpy(file, update->filename, ELFNAME_LEN);
    elf_size = update->elf_size;
    PRINTF("Update with file %s of size %d \n", file, elf_size);
    fd = cfs_open(file, CFS_READ);
    if(fd < 0) {
      printf("#UPDATE# %u.%u:File %s not found #!UPDATE#\n", SINK_ADDR_0, SINK_ADDR_1, file);
      continue;
      //process_exit(NULL);
    }

    if(cfs_seek(fd, elf_size, CFS_SEEK_SET) != elf_size) {
      printf("failed to seek to the end\n");
    }
    res = deluge_disseminate(file, node_id == SINK_ADDR_0);
    printf("#UPDATE# %u.%u:STARTED->%s/%u #!UPDATE#\n", SINK_ADDR_0, SINK_ADDR_1, file, elf_size);
    PRINTF("res = %d\n", res);
    cfs_close(fd);

  }

  PROCESS_END();
}

/*---------------------------------------------------------------------------*/
