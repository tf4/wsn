#define MAX_APPDATA_SIZE 100

#define TEMP	0
#define HUM	1
#define LIGHT1	2
#define LIGHT2	3
//#define SENSOR_NUM	4

struct app_data_msg_struct {
  uint8_t from;
  uint8_t length;
  unsigned char values[MAX_APPDATA_SIZE];
};


char *value_names[4] = {"temp", "hum", "light1", "light2"};
char *data_prefix[3] = {"APPDATA", "PRD_REQ", "QRY_REQ"};

typedef union  {
  float f;
  unsigned long u;
}ufloat;
