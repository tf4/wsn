# README #


### Sample code ###

* Parts of the firmware code for a WSN
* ContikiOS files are not included


### Leaf node firmware ###

* Firmware of nodes responsible for acquiring measurements
* node/node.c
* Functionality is split into processes
* The middleware consists of the *main*, *sensor* and *file transfer* processes
* The user application layer is a separate process which is executed only if a user loads an ELF file on the node
* The main process thread is accepting events from the RF for incoming messages and executes the corresponding commands. Depending upon which command was received, *main* notifies the appropriate process. It also accepts events from all the other processes that have data for the coordinator.
* For sensor operation the *sensor_process* is notified
* For file transfer operation the *file_transfer_process* is notified
* For user-defined operations the application layer process is notified
* Execution start is at 
```
#!c

PROCESS_THREAD(main_process, ...)
```
 -->node/node.c-->line 458

### Application layer ###

* template.c
* Provides an interface for implementing simple user customized applications without having to know anything about the middleware. The API links the application with the lower layers and provides a small set of basic functions. Users just need to implement the abstract functions.
* The application process must be compiled into an ELF file (*.ce) and loaded on the external flash of the node either manually or by the wireless file transfer protocol.
It can be removed or substituted at any time
* All nodes must run the same firmware/middleware but each node can run a different application process
* template.c contains only one process the *elf_process* .The connection with the rest of the firmware is achieved through this process, which sends events to the *main_process* whenever it is necessary

### Coordinator (sink-node) ###

* Accepts text commands from the serial port and forwards them to the target nodes
* Receives data messages from nodes and forwards them as strings to the serial port
* *command_dispatcher_process* (coordinator/sink.c--->line 378) and *recv_runicast* callback function (coordinator/sink.c--->line 230) are actually the starting points of any operation of the coordinator
* shell-app.c is an app that translates user input from the serial port into an understandable format for the coordinator
* Text commands are translated into structures and then passed as arguments on events that finally get handled by the *command_dispatcher_process* of the coordinator.