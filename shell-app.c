/*
 * A library to be used as an extension of the COntiki Shell
 * It enables the coordinator node to accept and recognize text commands
 * from the serial port.
 *
 */



#include "contiki.h"
#include "kripis.h"
#include "shell-app.h"
#include "cfs/cfs.h"

#include <stdio.h>
#include <string.h>

#define MAX_FILENAME_LEN 40
#define MAX_BLOCKSIZE 40
#define MAX_ELFNAME_LEN	6

char *sensor_names[SENSOR_NUM] = {"temp", "hum", "light1", "light2"};
process_event_t event_shell_app_command;

/*---------------------------------------------------------------------------*/
PROCESS(shell_setrate_process, "setrate");
SHELL_COMMAND(setrate_command,
	      "setrate",
	      "setrate [nodeid0] [nodeid1] [sht_rate] [light_rate]: set sample sht_rate as sht_rate in node with nodeid",
	      &shell_setrate_process);
PROCESS(shell_noderbt_process, "noderbt");
SHELL_COMMAND(noderbt_command,
	      "noderbt",
	      "noderbt [nodeid0] [nodeid1]: reboot node with nodeid",
	      &shell_noderbt_process);
PROCESS(shell_elfupdate_process, "elfupdate");
SHELL_COMMAND(elfupdate_command,
	      "elfupdate",
	      "elfupdate [nodeid0] [nodeid1] <elf_filename> [elf_filesize]: load an elf file to a node",
	      &shell_elfupdate_process);
PROCESS(shell_setapp_process, "setrate");
SHELL_COMMAND(setapp_command,
	      "setapp",
	      "setapp [nodeid0] [nodeid1] [p0] [p1] [p2] [p3] [p4] [p5]: set elf application parameters",
	      &shell_setapp_process);
PROCESS(shell_query_process, "query");
SHELL_COMMAND(query_command,
	      "query",
	      "query [nodeid0] [nodeid1] [sensor_bitmask]",
	      &shell_query_process);
PROCESS(shell_nbr_process, "nbr");
SHELL_COMMAND(nbr_command,
	      "nbr",
	      "nbr",
	      &shell_nbr_process);
PROCESS(shell_nodeconf_process, "nodeconf");
SHELL_COMMAND(nodeconf_command,
	      "nodeconf",
	      "nodeconf [nodeid0] [nodeid1] [tx] [piggy]",
	      &shell_nodeconf_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_setrate_process, ev, data)
{
  static int nodeid0 = 0;
  static int nodeid1 = 0;
  int index = 0;
  static struct cmd_packet command;
  char *next;
  PROCESS_BEGIN();


  if(data != NULL) {
    next = strchr(data, ' ');
    nodeid0 = shell_strtolong(data, NULL);
    if(next == NULL) {
      shell_output_str(&setrate_command, "Missing rime_addr[1] parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      nodeid1 = shell_strtolong(next, NULL);
    }
    while(index < SENSOR_NUM) {
      next = strchr(next, ' ');
      if(next != NULL) {
	command.ctrl_msg.cmd.sample_rates.sensor_rates[index] = shell_strtolong(next, NULL);
	index++;
	next++;
      }
      else {
	//TODO: This should return the sensor id number that will be matched to its name according to the xml file on the GW
	shell_output_str(&setrate_command, "Missing rate parameter ", sensor_names[index]);
	PROCESS_EXIT();
      }
    }

    command.addr.u8[0] = (unsigned char)nodeid0;
    command.addr.u8[1] = (unsigned char)nodeid1;
    command.ctrl_msg.cmd_type = CMD_SETRATES;
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&setrate_command, "Error: No parameters", "");
    PROCESS_EXIT();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_noderbt_process, ev, data)
{
  static struct cmd_packet command;
  static int nodeid0 = 0;
  static int nodeid1 = 0;
  char* next;

  PROCESS_BEGIN();

  if(data != NULL) {
    next = strchr(data, ' ');
    nodeid0 = shell_strtolong(data, NULL);
    if(next == NULL) {
      shell_output_str(&noderbt_command, "Missing rime_addr[1] parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      nodeid1 = shell_strtolong(next, NULL);
    }
    command.addr.u8[0] = (unsigned char)nodeid0;
    command.addr.u8[1] = (unsigned char)nodeid1;
    command.ctrl_msg.cmd_type = CMD_REBOOT;
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&noderbt_command, "Error: No nodeid parameters", "");
    PROCESS_EXIT();
  }
  PROCESS_END();
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_elfupdate_process, ev, data)
{
  static struct cmd_packet command;
  static int nodeid0 = 0;
  static int nodeid1 = 0;
  static char filename[MAX_FILENAME_LEN];
  static int filesize = 0;
  char* next;
  char* namestart;
  int len;

  PROCESS_BEGIN();
  
  if(data != NULL) {
    next = strchr(data, ' ');
    nodeid0 = shell_strtolong(data, NULL);
    if(next == NULL) {
      shell_output_str(&elfupdate_command, "#UPDATE# Missing rime_addr[1] parameter #!UPDATE#", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      namestart = strchr(next, ' ');
      nodeid1 = shell_strtolong(next, NULL);
    }
    if(namestart == NULL) {
      shell_output_str(&elfupdate_command, "#UPDATE# Missing elf filename #!UPDATE#", "");
      PROCESS_EXIT();
    }
    else {
      namestart++;
      next = strchr(namestart, ' ');
      if(next == NULL) {
        shell_output_str(&elfupdate_command, "#UPDATE# Missing elf size #!UPDATE#", "");
        PROCESS_EXIT();
      }
      else {
        len = (int)(next - namestart);
        if(len != MAX_ELFNAME_LEN) {
	shell_output_str(&elfupdate_command,
		       "#UPDATE# elfupdate: filename must be 6 characters long: #!UPDATE#", "");
	PROCESS_EXIT();
        }
        memcpy(filename, namestart, len);
        filesize = shell_strtolong(next, NULL);
	next++;
	namestart = strchr(next, ' ');
	if(namestart != NULL) {
	  if(strncmp(namestart, " l", 2) == 0) {
	    filesize = -1*filesize;
	  }
	}
      }
      
    }
    command.addr.u8[0] = (unsigned char)nodeid0;
    command.addr.u8[1] = (unsigned char)nodeid1;
    command.ctrl_msg.cmd_type = CMD_UPDATE;
    strncpy(command.ctrl_msg.cmd.elf_update.filename, filename, MAX_ELFNAME_LEN);
    command.ctrl_msg.cmd.elf_update.filename[6] = '.';
    command.ctrl_msg.cmd.elf_update.filename[7] = 'c';
    command.ctrl_msg.cmd.elf_update.filename[8] = 'e';
    command.ctrl_msg.cmd.elf_update.filename[9] = '\0';
    command.ctrl_msg.cmd.elf_update.elf_size = filesize;
    command.ctrl_msg.cmd.elf_update.status = 0;
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&elfupdate_command, "#UPDATE# Error: No nodeid parameters #!UPDATE#", "");
    PROCESS_EXIT();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(shell_setapp_process, ev, data)
{
  int index = 0;
  static struct cmd_packet command;
  unsigned long value;
  char *next;
  PROCESS_BEGIN();
  memset(command.ctrl_msg.cmd.user_app_cmd.pm, 0, 32);

  if(data != NULL) {
    next = strchr(data, ' ');
    command.addr.u8[0] = (unsigned int)shell_strtolong(data, NULL);
    if(next == NULL) {
      shell_output_str(&setapp_command, "Missing rime_addr[1] parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      command.addr.u8[1] = (unsigned int)shell_strtolong(next, NULL);
      command.ctrl_msg.cmd_type = CMD_APPCMD;
    }
    next = strchr(next, ' ');
    if(next == NULL) {
      shell_output_str(&setapp_command, "Missing user command id parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      command.ctrl_msg.cmd.user_app_cmd.cmd_id = (uint8_t)shell_strtolong(next, NULL);
    }
    while(index < sizeof(command.ctrl_msg.cmd.user_app_cmd.pm)) {
      next = strchr(next, ' ');
      if(next != NULL) {
	value = (unsigned long)shell_strtolong(next, NULL);
	memcpy((void*)(&command.ctrl_msg.cmd.user_app_cmd.pm[index]), (void*)(&value), sizeof(unsigned long));
	index += sizeof(unsigned long);//index++;
	next++;
      }
      else {
	break;
      }
    }
    
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&setapp_command, "Error: No parameters", "");
    PROCESS_EXIT();
  }

  PROCESS_END();
}

PROCESS_THREAD(shell_query_process, ev, data)
{
  static struct cmd_packet command;
  char *next;
  PROCESS_BEGIN();


  if(data != NULL) {
    next = strchr(data, ' ');
    command.addr.u8[0] = (unsigned int)shell_strtolong(data, NULL);
    if(next == NULL) {
      shell_output_str(&query_command, "Missing rime_addr[1] parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      command.addr.u8[1] = (unsigned int)shell_strtolong(next, NULL);
      command.ctrl_msg.cmd_type = CMD_QUERY;
    }
    next = strchr(next, ' ');
    if(next != NULL) {
      command.ctrl_msg.cmd.bitmask = (uint8_t)shell_strtolong(next, NULL);
      next++;
    }
    else {
      shell_output_str(&query_command, "Missing bitmask parameter ", "");
      PROCESS_EXIT();
    }
    
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&query_command, "Error: No parameters", "");
    PROCESS_EXIT();
  }

  PROCESS_END();
}

PROCESS_THREAD(shell_nbr_process, ev, data)
{
  static struct cmd_packet command;
  PROCESS_BEGIN();
  command.addr.u8[0] = 4;
  command.addr.u8[1] = 0;
  command.ctrl_msg.cmd_type = CMD_NETSTAT;
  process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  PROCESS_END();  
}

PROCESS_THREAD(shell_nodeconf_process, ev, data)
{
  static struct cmd_packet command;
  char* next;

  PROCESS_BEGIN();

  if(data != NULL) {
    next = strchr(data, ' ');
    command.ctrl_msg.cmd_type = CMD_CONFIG;
    command.addr.u8[0] = (unsigned char)shell_strtolong(data, NULL);
    //printf("rime_addr[0] %d\n", nodeid0);
    if(next == NULL) {
      shell_output_str(&nodeconf_command, "Missing rime_addr[1] parameter", "");
      PROCESS_EXIT();
    }
    else {
      next++;
      command.addr.u8[1] = (unsigned char)shell_strtolong(next, NULL);
      //printf("rime_addr[1] %d\n", nodeid1);
    }
    next = strchr(next, ' ');
    if(next != NULL) {
      command.ctrl_msg.cmd.node_config_cmd.bitmask = (uint8_t)shell_strtolong(next, NULL);
      next++;
      next = strchr(next, ' ');
      if(next != NULL) {
	command.ctrl_msg.cmd.node_config_cmd.tx_power = (uint8_t)shell_strtolong(next, NULL);
      }
      next++;
      next = strchr(next, ' ');
      if(next != NULL) {
	command.ctrl_msg.cmd.node_config_cmd.status_info_freq = (uint16_t)shell_strtolong(next, NULL);
      }
    }
    else {
      shell_output_str(&nodeconf_command, "Missing config parameters", "");
      PROCESS_EXIT();
    }
    process_post(PROCESS_BROADCAST, PROCESS_EVENT_COMMAND, &command);
  }
  else {
    shell_output_str(&nodeconf_command, "Error: No nodeid parameters", "");
    PROCESS_EXIT();
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------------------*/
void
shell_app_init(void)
{
  shell_register_command(&setrate_command);
  shell_register_command(&noderbt_command);
  shell_register_command(&elfupdate_command);
  shell_register_command(&setapp_command);
  shell_register_command(&query_command);
  shell_register_command(&nbr_command);
  shell_register_command(&nodeconf_command);
}
/*---------------------------------------------------------------------------*/
