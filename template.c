
#include "contiki.h"
#include "datatypes.h"
//#include "dev/leds.h"

#include <stdio.h> /* For PRINTF() */
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif
//API functions. 
#define PACKET &msg
#define readUint8(arg1, arg2) _read_param(arg1, (void*)(&params.pm[arg2]), sizeof(uint8_t))
#define readUint16(arg1, arg2) _read_param(arg1, (void*)(&params.pm[arg2]), sizeof(uint16_t))
#define readInt(arg1, arg2) _read_param(arg1, (void*)(&params.pm[arg2]), sizeof(int))
#define readFloat(arg1, arg2) _read_param(arg1, (void*)(&params.pm[arg2]), sizeof(float))
#define readUlong(arg1, arg2) _read_param(arg1, (void*)(&params.pm[arg2]), sizeof(unsigned long))
#define writeUint8(arg1, arg2) _set_values(arg1, PACKET, sizeof(uint8_t), arg2)
#define writeUint16(arg1, arg2) _set_values(arg1, PACKET, sizeof(uint16_t), arg2)
#define writeInt(arg1, arg2) _set_values(arg1, PACKET, sizeof(int), arg2)
#define writeFloat(arg1, arg2) _set_values(arg1, PACKET, sizeof(float), arg2)
#define writeUlong(arg1, arg2) _set_values(arg1, PACKET, sizeof(unsigned long), arg2)
#define get_cmd_type() params.cmd_id
#define get_sensors()  data->sensor_mask
#define get_sensor_val(arg1)  data->values[arg1]
#define get_battery()	data->bt
#define get_timestamp()	data->timestamp
#define get_txnum()	data->tx_num

#define MAX_APPDATA_SIZE 100
struct app_data_msg {
  uint8_t from;
  uint8_t length;
  unsigned char values[MAX_APPDATA_SIZE];
};

/*---------------------------------------------------------------------------*/
PROCESS(elf_process, "elf process");
AUTOSTART_PROCESSES(&elf_process);
/*---------------------------------------------------------------------------*/

//API global variables
static struct cmd_packet ctrl_msg;
static struct cmd_packet ack_ctrl_msg;
static struct data_container_struct wrapped_msg;
static struct app_data_msg msg;
static unsigned int msg_size;
static uint8_t version_id;
//--------------API functions-------------------

/*Set sampling rates for sensors*/
static void
post_rates(uint16_t rates[SENSOR_NUM]) {
  ctrl_msg.u_cmd_type = CMD_APPCMD;
  int i;
  for(i = 0; i < SENSOR_NUM; i++) {
    ctrl_msg.cmd.samplerates.sensor_rates[i] = rates[i];
  }
  process_post(PROCESS_BROADCAST, PROCESS_EVENT_APPCOMMAND, &ctrl_msg);
}

/*Send data message*/
static void
send_msg() {
  wrapped_msg.msg = (void*)(&msg);
  wrapped_msg.msg_size = msg_size;
  process_post(PROCESS_BROADCAST, PROCESS_EVENT_APPDATA, &wrapped_msg);
}

/*Clear payload*/
static void
clear_msg() {
  uint8_t from;
  from = msg.from;
  memset(&msg, 0, sizeof(struct app_data_msg));
  msg.from = from;
  msg_size = 0;
}

//Don't use these functions in application code. Use the macros instead
/*Write a value at a specific point in the payload buffer*/
static uint8_t
_set_values(void* data, struct app_data_msg* msg, unsigned int data_size, unsigned int offset) {
  unsigned int new_size = 0;
  if(offset + data_size > MAX_APPDATA_SIZE) {
    return 0;
  }
  new_size = sizeof(struct app_data_msg) - MAX_APPDATA_SIZE + data_size + offset;
  if(msg_size < new_size) {	//This keeps the correct size even if we write in a position lower that the maximum
    msg_size = new_size;
  }
  memcpy(msg->values + offset, data, data_size);
  msg->length = data_size + offset;
  return 1;
}

/*Read a parameter at a specific point at of an incoming message*/
static void
_read_param(void* p, void* buf, unsigned int param_size) {
  unsigned char temp_buf[4];
  long mask = 0xFF000000;
  uint8_t i;
  memcpy((void*)temp_buf, buf, param_size);
  if(param_size == 2) {
    mask = mask >> 16;
    *((uint16_t*)p) = 0;
    for(i = 0; i < param_size; i++) {
      *((uint16_t*)p) |= (((uint16_t)temp_buf[i])<<((param_size-1-i)*8))&(mask>>(i*8));
    }
  }
  else if(param_size == 4) {
    *((long*)p) = 0;
    for(i = 0; i < param_size; i++) {
      *((long*)p) |= (((long)temp_buf[i])<<((param_size-1-i)*8))&(mask>>(i*8));
    }
  }
  else if(param_size == 1) {
    memcpy(p, (void*)temp_buf, 1);
  }
}

//----------------------------------------------
/*User globals*/
//Define any of your globals here


/*---------------User functions-----------------------*/
//Define any of your functions here


//--------------API abstract methods------------------
static void
on_init() {
  //Enter user code here for handling application intialization
}

static uint8_t
on_command(app_params params) {
  //Enter user code here for handling an incoming command
  return 30
}

static void
on_sensor_data(struct sensor_values* data) {
  //Enter user code here for handling sensor values
}
//----------------------------------------------------

//Application process
PROCESS_THREAD(elf_process, ev, data)
{
  PROCESS_BEGIN();
  process_post(PROCESS_BROADCAST, PROCESS_EVENT_VERSION, &version_id);
  on_init();
  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_SENSOR_VALUES || ev == PROCESS_EVENT_COMMAND);
    if(ev == PROCESS_EVENT_SENSOR_VALUES) {
      msg.from = ((struct sensor_values *)data)->from;
      on_sensor_data((struct sensor_values *)data);
    }
    else {
      if(((struct ctrl_type_msg*)data)->cmd_type == CMD_APPCMD) {
	uint8_t res;
	res = on_command(((struct ctrl_type_msg*)data)->cmd.params);
	ack_ctrl_msg.cmd_type = CMD_APPCMD;
	ack_ctrl_msg.cmd.bitmask = res;
	//process_post(PROCESS_BROADCAST, PROCESS_EVENT_APPCOMMAND, &ack_ctrl_msg);
	pr_post(&ack_ctrl_msg);
      }
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
