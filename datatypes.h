#include "net/rime.h"
/*define the address of the coordinator node (4.0)*/
#define SINK_ADDR_0	4
#define SINK_ADDR_1	0

//Define packet types
#define CONTROL_PACKET 0
#define DATA_PACKET 1
//define message data types
#define USER_APPLICATION_DATA		0
#define PERIODIC_QUERY_DATA		1
#define SINGLE_QUERY_DATA		2

// Command types
#define CMD_SETRATES	0 //for setting periodic query rates
#define CMD_UPDATE	1 //for updating ELF executable
#define CMD_REBOOT	2 //for soft reboot
#define CMD_QUERY 	3 //for single query
#define CMD_NETSTAT	4
#define CMD_CONFIG	5//for configuring node parameters

//Error codes for ELF file update
#define UPDATE_SUCCESS	20
#define DELUGE_STARTED	21
#define DELUGE_EXIT	22
#define DELUGE_BUSY	23
#define READ_ERROR	24
#define WRITE_ERROR	25
#define COPY_ERROR	26
#define DELUGE_ERROR	27
#define LOADER_ERROR	28
#define LOCAL_UPDATE	29

//User application related command types
#define CMD_SETAPPRATES	30//for setting query rates within a user application
#define CMD_APPCMD	31//Commands targeted to a user application currentl installed

#define MAX_DATA_SIZE	90 // Maximum size of data payload
#define SENSOR_NUM 4 //Number of installed sensor on the node

//Command related structures

//Structure carrying a command targeted to a user application
typedef struct {
  uint8_t cmd_id; //Command id
  unsigned char pm[32]; //Byte array with command parameters
}user_app_cmd_struct;

//Structure carrying sampling rates for periodic query command/request
typedef struct {
  uint16_t sensor_rates[SENSOR_NUM];
}sample_rates_struct;

//Structure carrying parameters for an ELF file update command
typedef struct {
  char filename[10];
  int elf_size;
  uint8_t status;
}elf_update_struct;

//Structure carrying parameters for a node configuration command
typedef struct {
  uint8_t bitmask; //set bits to select which parameters will be updated
  uint8_t tx_power; //transmission power
  uint16_t status_info_freq; //how frequently operational status messages are transmitted by the node
  uint16_t piggy_bag; // feature not implemented yet
} node_config_cmd_struct;

//Structure carrying operational status info about the node
typedef struct {
  uint8_t version; //User application version
  unsigned long timestamp; //Elapsed time since last (re)boot
  uint8_t deluge_status; //Deluge file transfer protocol status (on/off)
  uint8_t tx_power; //Current transmission power level
  uint16_t tx_packets; //Total number of transmitted packets
  float battery; //Battery voltage
} operational_status_info_struct;

//Union of all possible command structures 
typedef union {
  uint8_t bitmask; // used in cases where only the cmd_type field is enough
  operational_status_info_struct operational_status_info;
  sample_rates_struct sample_rates;
  elf_update_struct elf_update;
  user_app_cmd_struct user_app_cmd;
  node_config_cmd_struct node_config_cmd;
} cmd_union;

// Structure containing control message (wrapped command settings and command type)
struct ctrl_msg_struct {
  uint8_t cmd_type; 
  cmd_union cmd;
};

/*
 * Encapsulates a cmd message into a packet structure containing the destination rime address.
 * This encapsulation takes place when the command is received at the serial port only.
 * The packet is de-encapsulated when it is handled by the main process of the coordinator
 * After that any other kind of encapsulation is handed by Rime protocol
 */
struct cmd_packet {
  rimeaddr_t addr;
  struct ctrl_msg_struct ctrl_msg;
};

//Data related structures

// Structure containing data message
struct data_msg_struct {
  uint8_t from; //identifies the process which created the data
  uint8_t packet_seq_no;
  uint8_t sensor_mask; //bitmask showing which sensors were sampled
  uint8_t padding;
  unsigned long timestamp; //Time data was collected
  float bt; //batter value
  unsigned char values[MAX_DATA_SIZE]; //Actual data
};

// Struct for wrapping data to be sent
// Can contain single or periodic query data. user app data or both
struct data_container_struct {
  void* msg;
  int msg_size;
};

//Structure carrying sensor data. Used for inter-proccess communication on the node
struct __attribute__((__packed__)) sensor_values {
  uint8_t from; //Indicates whether data was requested by a periodic query, single query or user application query
  uint8_t sensor_mask; // Bitmask indicating which sensors were sampled
  unsigned long timestamp; //Time of measurement
  float bt; //Battery value
  float values[SENSOR_NUM]; // Actual sensor data
  uint16_t tx_num; //Number of total transmissions
  //TODO:Add array of raw adc values also
};

void pr_post(struct ctrl_msg_struct* ack_m);