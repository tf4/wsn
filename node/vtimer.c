#include "vtimer.h"

/*
 * Set a virtual timer.
 * ref_rate is the value of a real timer and rate is the value of the virtual timer
 * The counter controls the virtual timer expiration
 */
void vtimer_set(vtimer* vt, unsigned int rate, unsigned int ref_rate) {
  vt->rate = rate;
  vt->ref_rate = ref_rate;
  vt->counter = 0;
}

/*Force the timer to tick once*/
void vtimer_tick(vtimer* vt) {
  vt->counter++;
}

void vtimer_reset(vtimer* vt) {
  vt->counter = 0;
}

/*Check if the virtual timer has expired*/
uint8_t vtimer_expired(vtimer* vt) {
  if((vt->counter*vt->ref_rate)%vt->rate == 0 && vt->rate > 0) {
    vtimer_reset(vt);
    return 1;
  }
  else { return 0; }
}

/*Force the virual timer to expire at the next check */
void vtimer_force(vtimer* vt) {
  vt->counter = -1;
}

/*Synchronize an already active vtimer according to a new reference value of a real timer*/
void vtimer_sync(vtimer* vt, struct ctimer* ct, unsigned int new_ref_rate) {
  vt->counter = ((vt->counter)*vt->ref_rate) / new_ref_rate;
  vt->ref_rate = new_ref_rate;
}
