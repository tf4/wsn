/*#include "net/rime.h"


#define SINK_ADDR_0	4
#define SINK_ADDR_1	0*/
#include "datatypes.h"


// Low level library functions that need to be linked
extern int __mulsf3();
extern int __udivmodhi4();
extern int __subsf3();
extern int __floatunsisf();
extern int __fixunssfsi();
extern int __addsf3();

//extern process_event_t event_datapacket_tosend;

#define MAX_PAYLOAD_SZ	103 //Actual data payload is 90, because some bytes are used for encapsulation purposes

// Sensor selector identifiers
#define SHT1_USR_VT	0
#define SHT2_USR_VT	1
#define LIGHT1_USR_VT	2
#define LIGHT2_USR_VT	3
#define SHT1_APP_VT	4
#define SHT2_APP_VT	5
#define LIGHT1_APP_VT	6
#define LIGHT2_APP_VT	7
#define TIMER_NUM	8
//Process selector identifiers
#define SINGLE_QUERY_PROCESS	0x04
#define PERIODIC_QUERY_PROCESS	0x02
#define APP_QUERY_PROCESS	0x01

#define TEMP	0
#define HUM	1
#define LIGHT1	2
#define LIGHT2	3

typedef struct {
    uint8_t std_sensor_mask;
    uint8_t app_sensor_mask;
    uint8_t target_process_mask;
  }mask_struct;


unsigned int create_packet(void* data, struct data_msg_struct* msg, int data_size, int offset);
unsigned int gcd(unsigned int u, unsigned int v);