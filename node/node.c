/*
 * 
 */
/*! \file */ 
#include "contiki.h"
#include "node.h"
#include "sys/node-id.h"
#include "dev/watchdog.h" //Use watchdog_reboot for soft reset
#include "vtimer.h"
#include "sys/rtimer.h"
#include "net/rime.h"
#include "lib/list.h"
#include "dev/cc2420.h"
#include "dev/button-sensor.h"
#include "dev/battery-sensor.h"
#include "dev/light-sensor.h"	//lux=2,5*((sensor data)/4096)*6250
#include "dev/sht11-sensor.h"
#include "dev/leds.h"
#include "deluge.h"
#include "cfs/cfs.h"
#include "cfs/cfs-coffee.h"
#include "loader/elfloader.h"
#include "sensor-functions.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define DEBUG 0
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define NO_LEDS 1
#if NO_LEDS
#define leds_toggle(arg) leds_off(arg)
#endif

#define CHANNEL 135

#define DEFAULT_STATUS_INFO_FREQ 120
#define MAX_NEIGHBORS 1
#define MAX_RETRANSMISSIONS 0
#define NUM_HISTORY_ENTRIES 4

#define MAX_BUFFERED_DATA	12

#define ANNOUNCE_TIME	10
#define STATUS_INFO_CONFIG 2
#define TX_POWER_CONFIG	1

#define BUF_SIZE 256

//Header attributes regarding the application layer are added in runicast.h
/*#define APP_ATTRIBUTES        { PACKETBUF_ATTR_EPACKET_TYPE, PACKETBUF_ATTR_BIT }, \
                                MULTIHOP_ATTRIBUTES*/
static clock_time_t start_test_m = 0;
static clock_time_t stop_test_m = 0;
// Declare and set initial rate of measurements
static struct s_rates {
  //8 Virtual timers. 4 for the sensor rates of standard periodic queries and another 4 for application sensor queries
  vtimer vt[TIMER_NUM]; 
  vtimer prd_gen_vt; //this timer is essential for keeping track of a sequence id that can produce the correct time when multiplied with gcd
  unsigned int gcd_rate; //Global greatest common divider for all sampling rates
  unsigned int prd_gcd_rate; //Greatest common divider of sensor sampling rate of standard periodic query
  unsigned int app_gcd_rate; //Greatest common divider of sensor sampling rates of user application queries
}rates;

static struct ctimer ct; //Callback timer controlling sensor sampling
static struct etimer status_info_etimer; //Timer for controlling transmission of periodic status information messages
static uint16_t status_info_freq;

static uint8_t prd_request_on = 0;//Flag indicating a standard periodic query is active
static uint16_t prd_request_seq_id = 1; //Keeping track of the standard periodic sequence
static uint8_t app_request_on = 0;//Flag indicating a user application periodic query is active
static uint8_t app_request_seq_id = 0; //TODO: Possibly redundant
static uint8_t version_id = 0; //Currently intalled user app id
static uint16_t tx_num = 0; //Number of total node transmissions
static elf_update_struct update_struct; //For storing received file info regarding an incoming ELF file

//Not used currently
static void* data_ptr[MAX_BUFFERED_DATA];
static int data_sizes[MAX_BUFFERED_DATA];
static uint8_t data_counter = 0;
static void* data_payload;
static int data_payload_size = 0;

//Events
static process_event_t event_update_done; //Notifies about the state of an ELF file transfer and installation
static process_event_t event_runicast_send; //Triggers a runicast transmission
static process_event_t event_runicast_finished; //Notifies that a transmission is complete
//static process_event_t event_update_done_rc;
//static process_event_t event_update_done;
/*---------------------------------------------------------------------------*/
PROCESS(main_process, "main");
PROCESS(file_transfer_process, "dlg");
PROCESS(sensor_process, "sns");
//PROCESS(data_handler_process, "dh");
PROCESS(runicast_wait, "wt");
AUTOSTART_PROCESSES(&main_process, &sensor_process);//, &file_transfer_process);
/*---------------------------------------------------------------------------*/


struct history_entry {
  struct history_entry *next;
  rimeaddr_t addr;
  uint8_t seq;
};
LIST(history_table);
MEMB(history_mem, struct history_entry, NUM_HISTORY_ENTRIES);

/*Function to be used by the application API in order to post synchronous events to main process*/
void pr_post(struct ctrl_msg_struct* ack_m) {
  process_post_synch(&main_process, PROCESS_EVENT_APPCOMMAND, ack_m);
}

/*
 * The callback function of a timer that controls sampling
 */
static void
timercb(void *data){
  int i;
  mask_struct masks = {0};
  uint8_t flag = 0;
  if(prd_request_on > 0) {
    vtimer_tick(&rates.prd_gen_vt);
    if(vtimer_expired(&rates.prd_gen_vt)) {
      prd_request_seq_id += 1;
    }
  }
  //Increase virtual timers and check if any of them expired
  //Timers from 0 to 3 are used for standard periodic queries
  //Timers from 4 to 7 are used for application periodic queries
  for(i=0; i < TIMER_NUM; i++) {
    vtimer_tick(&rates.vt[i]);
    if(vtimer_expired(&rates.vt[i])) {
      // Set selected processes in bitmask
      if(i < TIMER_NUM/2) {
	flag = 1; //flag for correcting sequence id
	//Set the bitmask for the sensor that is related to the expired vtimer
	masks.std_sensor_mask |= (0x01<<i);
	masks.target_process_mask = PERIODIC_QUERY_PROCESS;
      }
      else {
	//Set the bitmask for the sensor that is related to the expired vtimer
	masks.app_sensor_mask |= (0x01<<(i%SENSOR_NUM));
	masks.target_process_mask |= APP_QUERY_PROCESS;
      }
    }
  }
  //This is for correcting sequence id so that it will not be increased twice
  if(flag > 0) {
    prd_request_seq_id--;
  }
  //If one or more vtimers have expired notify the sensor process for getting measurements
  //Pass the mask so that it knows which sensors to activate and to which process it should deliver the results
  if(masks.std_sensor_mask | masks.app_sensor_mask) {
    process_post(&sensor_process, PROCESS_EVENT_REQ_SENSOR, &masks);
  }
  else {
   ctimer_reset(&ct);
  }
}

static uint16_t
gcd_of_array(uint16_t arr[SENSOR_NUM]) {
  int i;
  uint16_t res = 0;
  for(i = 0; i < SENSOR_NUM; i++) {
    res = gcd(res,arr[i]);
  }
  return res;
}

static int 
setSamplingSchedule(int cmd_type, sample_rates_struct data) {
  //Force all virtual timers to expire before setting new rates
  if(cmd_type == CMD_SETRATES) {
    int i;
    vtimer_force(&rates.prd_gen_vt);
    for(i = 0; i < TIMER_NUM/2; i++) {
      vtimer_force(&rates.vt[i]);
    }
    //Since we have received new sampling rates, the virtual timers must be adjusted accordingly
    //Find the GCD of all rates to apply it to the real timer
    //The sampling rates are then passed to the virtual timers of every sensor
    rates.prd_gcd_rate = gcd_of_array(data.sensor_rates);
    rates.gcd_rate = gcd(rates.prd_gcd_rate, rates.app_gcd_rate);
    //Force the real timer to expire so that the new schedule can be applied
    timercb(NULL);
    //Set the new rates
    vtimer_set(&rates.prd_gen_vt, rates.prd_gcd_rate, rates.gcd_rate);
    for(i = 0; i < TIMER_NUM/2; i++) {
      vtimer_set(&rates.vt[i], data.sensor_rates[i], rates.gcd_rate);
    }
    for(i = SENSOR_NUM; i < TIMER_NUM; i++) {
      vtimer_sync(&rates.vt[i], &ct, rates.gcd_rate);
    }
    if(prd_request_on > 0) { // there will be an extra packet with previously set rates which must be numbered zero
      prd_request_seq_id = 0;
    }
    if(rates.prd_gcd_rate > 0) { 
      prd_request_on = 1;
    }
    else { 
      prd_request_on = 0;
      prd_request_seq_id = 1;
    }
    if(rates.gcd_rate > 0) {
      ctimer_set(&ct, CLOCK_SECOND*rates.gcd_rate, timercb, NULL);
    }
    else {
      ctimer_stop(&ct);
      prd_request_seq_id = 1;	//next time start counting packets from 1
    }
    PRINTF("Rates set to %i\n", i);
    for(i = 0; i < TIMER_NUM/2; i++) {
      PRINTF("%u ", data.sensor_rates[i]);
    }
    PRINTF("%i\n", rates.gcd_rate);
    return CMD_SETRATES;
  }
  //The same as above but this time for the case we receive sampling rates for a user application
  if(cmd_type == CMD_SETAPPRATES) {
    int i;
    for(i = SENSOR_NUM; i < TIMER_NUM; i++) {
      vtimer_force(&rates.vt[i]);
      //prd_request_seq_id = 0;
    }
    rates.app_gcd_rate = gcd_of_array(data.sensor_rates);
    rates.gcd_rate = gcd(rates.prd_gcd_rate, rates.app_gcd_rate);
    timercb(NULL);
    for(i = SENSOR_NUM; i < TIMER_NUM; i++) {
      vtimer_set(&rates.vt[i], data.sensor_rates[i-SENSOR_NUM], rates.gcd_rate);
    }
    vtimer_sync(&rates.prd_gen_vt, &ct, rates.gcd_rate);
    //prd_request_seq_id = 1;
    for(i = 0; i < TIMER_NUM/2; i++) {
      vtimer_sync(&rates.vt[i], &ct, rates.gcd_rate);
    }
    if(rates.app_gcd_rate > 0) { app_request_on = 1;}
    else { app_request_on = 0;}
    if(rates.gcd_rate > 0) {
      ctimer_set(&ct, CLOCK_SECOND*rates.gcd_rate, timercb, NULL);
    }
    else {ctimer_stop(&ct);}
    PRINTF("Rates set to ");
    for(i = SENSOR_NUM; i < TIMER_NUM; i++) {
      PRINTF("%i ", data.sensor_rates[i-SENSOR_NUM]);
    }
    PRINTF("%i\n", rates.gcd_rate);
    return CMD_SETAPPRATES;
  }
}


/*Accepts a bitmask indicating which sensors should be sampled
 *Notifies the sensor process about the request
 */
static void
query_sensors(uint8_t mask) {
  static mask_struct masks;
  masks.std_sensor_mask = mask;
  masks.target_process_mask = SINGLE_QUERY_PROCESS;
  process_post(&sensor_process, PROCESS_EVENT_REQ_SENSOR, &masks);
}

/*
 * This function executes a received command
 * Accepts a pointer to a command message structure. 
 * Returns a code (int) identifying which operation was executed (or failed)
 * Command type is recognized by checking the ctrl_msg_struct.cmd_type field.
 * Each cmd_type matches a certain kind of structure (ctrl_msg_struct.cmd field is a union) 
 */
static int 
command_exec(struct ctrl_msg_struct* msg) {
  //A command for setting sampling rates either for a periodic query or for a user application
  if(msg->cmd_type == CMD_SETRATES || msg->cmd_type == CMD_SETAPPRATES) {
    int res;
    //Reschedule timers in order to follow the new sampling scheme
    res = setSamplingSchedule(msg->cmd_type, msg->cmd.sample_rates);
    return res;
  }
  //A command for starting the file transfer process for an incoming ELF.
  else if (msg->cmd_type == CMD_UPDATE) {
    elf_update_struct = msg->cmd.elf_update; //Copy file info
    //If file size is 0, this means that this is a command for stopping a file transfer
    if(update_struct.elf_size == 0) {
      process_exit(&deluge_process);
      process_exit(&file_transfer_process);
      return DELUGE_EXIT;
    }
    //Start the file transfer protocol
    if(!process_is_running(&deluge_process)) {
      process_start(&file_transfer_process, NULL);
      return CMD_UPDATE;
    }
    //File transfer protocol is already busy. Return the corresponding error code
    else {
      return DELUGE_BUSY;
    }
  }
  //Command for a reboot
  else if(msg->cmd_type == CMD_REBOOT) {
    watchdog_reboot();
    return CMD_REBOOT;
  }
  //Command for performing a sensor single query
  else if(msg->cmd_type == CMD_QUERY) {
    query_sensors(msg->cmd.bitmask);
    return CMD_QUERY;
  }
  //Command for configuring node parameters
  else if(msg->cmd_type == CMD_CONFIG) {
    //Check the bitmask to decide which values need to be updated
    if(msg->cmd.node_config_cmd.bitmask & STATUS_INFO_CONFIG) {
      status_info_freq = msg->cmd.node_config_cmd.status_info_freq;
      etimer_set(&status_info_etimer, status_info_freq * CLOCK_SECOND);
    }
    if(msg->cmd.node_config_cmd.bitmask & TX_POWER_CONFIG) {
      cc2420_set_txpower(msg->cmd.node_config_cmd.tx_power);
    }
    return CMD_CONFIG;
  }
  //The command has already been executed by the user application layer. Just return the code number to the coordinator
  // and the rest is user specific
  else if(msg->cmd_type >= CMD_APPCMD)
    return msg->cmd.bitmask;
}

/*
 * This function is called at the final recepient of the message.
 * Here we just deliver received messages to the appropriate process
 */
static void
recv_runicast(struct runicast_conn *c, const rimeaddr_t *from, uint8_t seqno)
{
  //----------------------------------------------------------------------------------
  leds_toggle(LEDS_RED);
  if(packetbuf_attr(PACKETBUF_ATTR_EPACKET_TYPE) == CONTROL_PACKET) {
  // The packet contains control commands
    struct ctrl_msg_struct rcvd_msg;
    packetbuf_copyto(&rcvd_msg);
    //If not application commands, post received data to main_process to be handled there
    if((&rcvd_msg)->cmd_type < CMD_SETAPPRATES) {
      process_post(&main_process, PROCESS_EVENT_COMMAND, &rcvd_msg);
    }
    //Else post the data to the user application process
    else {
      int i;
      for(i = 0; elfloader_autostart_processes[i] != NULL; ++i) {
	process_post(elfloader_autostart_processes[i], PROCESS_EVENT_COMMAND, &rcvd_msg);
      }
    }
  }
}

static void
sent_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
  PRINTF("runicast message sent to %d.%d, retransmissions %d\n",
	 to->u8[0], to->u8[1], retransmissions);
  tx_num += retransmissions;
  process_post(&runicast_wait, event_runicast_finished, NULL);
}

static void
timedout_runicast(struct runicast_conn *c, const rimeaddr_t *to, uint8_t retransmissions)
{
  PRINTF("runicast message timed out when sending to %d.%d, retransmissions %d\n",
	 to->u8[0], to->u8[1], retransmissions);
  tx_num += retransmissions;
  process_post(&runicast_wait, event_runicast_finished, NULL);
}

/* Functions for elf files*/
static int 
copy_file(char* src, char* dest) {
  int fdr, fdw, i, size;
  int chunks = 0;
  char buf[BUF_SIZE];
  fdr = cfs_open(src, CFS_READ);
  size = cfs_seek(fdr, 0, CFS_SEEK_END);
  cfs_close(fdr);
  fdr = cfs_open(src, CFS_READ);
  if(fdr < 0){
    return 0;
  }
  chunks = size / BUF_SIZE;
  if(size > chunks*BUF_SIZE){
    chunks++;
  }
  cfs_coffee_reserve(dest, chunks*BUF_SIZE);
  fdw = cfs_open(dest, CFS_WRITE);
  for(i = 0; i < chunks; i++) {
    memset(buf, 0, sizeof(buf));
    cfs_read(fdr, buf, sizeof(buf));
    cfs_write(fdw, buf, sizeof(buf));
  }
  cfs_close(fdr);
  cfs_close(fdw);
PRINTF("Copy complete %u\n",sizeof(buf));
  return 1;
} 

static void
kill_processes() {
  /* Kill any old processes. */
  if(elfloader_autostart_processes != NULL) {
    autostart_exit(elfloader_autostart_processes);
  }
  //TODO: Cancel current app schedule
  //app_request_on = 0;
}

/*
 * Accepts a filename and passes the file to the ELF loader
 * Returns 1 if successful, 0 if not
 */
static int
load_component(char* data) {
  char *name = "elf.ce"; //Name of a temporary file
  int fd, ret;
  //We have to copy the file into a temporary one and use that one instead
  //This is because the loader sometimes messes up the file it loads and makes it unusable for the next time
  if(copy_file(data, name) == 0) {
    return 0;
  }
  kill_processes();
  fd = cfs_open(name, CFS_READ | CFS_WRITE);
  if(fd < 0) {
    return 0;
  } else {
    ret = elfloader_load(fd);
    cfs_close(fd);
    if(ret == ELFLOADER_OK) {
      int i;
      for(i = 0; elfloader_autostart_processes[i] != NULL; ++i) {
	PRINTF("process %s\n", elfloader_autostart_processes[i]->name);
      }
      autostart_start(elfloader_autostart_processes);
      ret = 1;
    }
    else { ret = 0;}
  }
  cfs_remove(name);
  return ret;
}

static const struct runicast_callbacks runicast_callbacks = {recv_runicast,
							     sent_runicast,
							     timedout_runicast};
static struct runicast_conn runicast;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(main_process, ev, data)
{
  PROCESS_EXITHANDLER(runicast_close(&runicast);)
  //Perform initializations
  PROCESS_BEGIN();
  //Allocate events
  event_runicast_finished = process_alloc_event();
  event_runicast_send = process_alloc_event();
  event_update_done_rc = process_alloc_event();
  event_update_done = process_alloc_event();
  //Copy the coordinator's address to a convenient structure
  static const rimeaddr_t sink_addr = {{SINK_ADDR_0, SINK_ADDR_1}};
  /* Open a runicast connection on Rime channel CHANNEL. */
  runicast_open(&runicast, CHANNEL, &runicast_callbacks);
  //Announnce new node to sink in ANNOUNCE_TIME secs
  etimer_set(&status_info_etimer, ANNOUNCE_TIME*CLOCK_SECOND);
  //Set the operational status messages frequency at the default value
  status_info_freq = DEFAULT_STATUS_INFO_FREQ;
  //Initialize the ELF loader
  elfloader_init();
  //Load application file external flash, if it exists.
  load_component("boot.ce");

  leds_toggle(LEDS_BLUE);
  while(1) {
    //Check for events
    PROCESS_YIELD();
    //Main process received data in order to pass it to the network stack for delivery
    if(ev == event_runicast_send) {
      //Set packet buffer length, attributes and copy the data into its payload
      packetbuf_set_datalen(((struct data_container_struct*)data)->msg_size);
      packetbuf_copyfrom(((struct data_container_struct*)data)->msg, ((struct data_container_struct*)data)->msg_size);
      packetbuf_set_attr(PACKETBUF_ATTR_EPACKET_TYPE, DATA_PACKET);
      // Send the packet to coordinator using reliable unicast.
      runicast_send(&runicast, &sink_addr, MAX_RETRANSMISSIONS);
      //DAta packets contain status info in the header, so it is better to restart the status info timer in order to avoid redundancy
      etimer_restart(&status_info_etimer);
    }
    //Main process received an event indicating that an ELF file was successfully received and installed
    else if(ev == event_update_done) {
      //Construct a message containing the ELF file info
      //When the coordinator receives it, it will understand that the specific file was successfully installed on the node
      struct ctrl_msg_struct *msg;
      packetbuf_clear();
      msg = (struct ctrl_msg_struct*)packetbuf_dataptr();
      packetbuf_set_datalen(sizeof(struct ctrl_msg_struct));
      msg->cmd_type = CMD_UPDATE;
      msg->cmd.elf_update = *(elf_update_struct*)data; 
      packetbuf_set_attr(PACKETBUF_ATTR_EPACKET_TYPE, CONTROL_PACKET);
      // Send the packet.
      runicast_send(&runicast, &sink_addr, MAX_RETRANSMISSIONS);
    }
    //Main process received a command from the coordinator
    else if(ev == PROCESS_EVENT_COMMAND || ev == PROCESS_EVENT_APPCOMMAND) {
      struct ctrl_msg_struct* msg;
      int res;
      //Execute the received command
      res = command_exec((struct ctrl_msg_struct*)data);
      packetbuf_clear();
      msg = (struct ctrl_msg_struct*)packetbuf_dataptr();
      //If the command was a periodic request, construct an ack message for the coordinator
      //which will contain the sampling rates, indicating that they were successfully applied
      if(res == CMD_SETRATES) {
	msg->cmd_type = CMD_SETRATES;
	int i;
	for(i = 0; i < SENSOR_NUM; i++) {
	  msg->cmd.sample_rates.sensor_rates[i] = rates.vt[i].rate;
	}
      }
      else if(res == CMD_SETAPPRATES) {
	continue;
      }
      //If the command was related to a deluge protocol operation and something went wrong 
      //notify the coordinator with the returned error code
      else if(res >= DELUGE_EXIT && res <= LOCAL_UPDATE) {
	msg->cmd_type = CMD_UPDATE;
	msg->cmd.elf_update.status = res;
      }
      //A user application operation command was executed. Construct an ACK for the coordinator
      //It will contain an error code that can be identified only by the user who created the application
      else if(res >= CCMD_APPCMD) {
	msg->cmd_type = CMD_APPCMD;
	msg->cmd.bitmask = res - CMD_SETAPPRATES;
      }
      //A node configuration command was executed. Construct an ACK with the newly applied parameters
      else if(res == CMD_CONFIG) {
	msg->cmd_type = CMD_CONFIG;
	msg->cmd.node_config_cmd.bitmask = 0;
	msg->cmd.node_config_cmd.tx_power = cc2420_get_txpower();
	msg->cmd.node_config_cmd.status_info_freq = status_info_freq;
	msg->cmd.node_config_cmd.piggy_bag = 0;
      }
      else {
	continue;
      }
      //If an ACK was constructed, send it to coordinator
      packetbuf_set_datalen(sizeof(struct ctrl_msg_struct));
      packetbuf_set_attr(PACKETBUF_ATTR_EPACKET_TYPE, CONTROL_PACKET);
      runicast_send(&runicast, &sink_addr, MAX_RETRANSMISSIONS);
    }
    //Main process received an event indicating that the status info timer expired
    else if(ev == PROCESS_EVENT_TIMER && etimer_expired(&status_info_etimer)) {
      //Construct a message with node status information
      //i.e. battery,tx power,current application id, deluge protocol status, timestamp, total number of transmitted packets
      SENSORS_ACTIVATE(battery_sensor);
      struct ctrl_msg_struct *msg;
      packetbuf_clear();
      msg = (struct ctrl_msg_struct*)packetbuf_dataptr();
      packetbuf_set_datalen(sizeof(struct ctrl_msg_struct));
      msg->cmd_type = CMD_NETSTAT;
      msg->cmd.operational_status_info.version = version_id;
      msg->cmd.operational_status_info.timestamp = (unsigned long)clock_time()/CLOCK_SECOND;
      msg->cmd.operational_status_info.deluge_status = process_is_running(&deluge_process);
      msg->cmd.operational_status_info.battery = batteryV(battery_sensor.value(0));
      msg->cmd.operational_status_info.tx_power = cc2420_get_txpower();
      msg->cmd.operational_status_info.tx_packets = tx_num;
      packetbuf_set_attr(PACKETBUF_ATTR_EPACKET_TYPE, CONTROL_PACKET);
      // Send the packet.
      runicast_send(&runicast, &sink_addr, MAX_RETRANSMISSIONS);
      //Start the status info timer again
      etimer_set(&status_info_etimer, status_info_freq*CLOCK_SECOND);
      SENSORS_DEACTIVATE(battery_sensor);
    }
    //Main process received an event from a newly installed application. Just stores the application ID
    else if(ev == PROCESS_EVENT_VERSION) {
      version_id = *((uint8_t*)data);
    }
  }
  PROCESS_END();
}

/*------------------------------------------------------------------------------*/
PROCESS_THREAD(runicast_wait, ev, data)
{
  /*
   * In some cases when the number of retransmissions is set more than 5, some packets are dropped
   * because the RF is still busy sending the previous one. In order to avoid that, a queue should be 
   * implemented here and post a packet to main process for deliver when RF is free
   * sent_runicast and timedout_runicast callbacks can be used
   */
  PROCESS_BEGIN();
  PROCESS_END();
}


/*-----------------------------------------------------------------------------------*/
PROCESS_THREAD(file_transfer_process, ev, data) {

  static char *file;
  static int fd = -1;
  static int res = -1;
  static int file_size = 0;
  static char buf[BUF_SIZE];
  static uint8_t otap_flag = 1;
  int chunks=0;
  
  PROCESS_BEGIN();
  
  file = update_struct.filename;
  file_size = update_struct.elf_size;
  update_struct.status = UPDATE_SUCCESS;
  //Delete the previously running ELF file (if exists)
  cfs_remove("boot.ce");
  //A negative file size value indicates that there won't be a file transfer.
  //The user is trying to install a file he has previously uploaded
  if(file_size < 0) {
    file_size = -1*file_size;
    update_struct.elf_size = file_size;
    otap_flag = 0;
    leds_toggle(LEDS_BLUE);
    res = copy_file(file, "boot.ce");
    if(res == 0) {	//inform that something went wrong when copying the file
      update_struct.status = COPY_ERROR;
      process_post(&main_process, event_update_done, &update_struct);
      cfs_remove(file);
      PROCESS_END();
    }
  }
  //Remove a file with the same name if exists
  cfs_remove(file);
  //For some reason the if a file's size is not a product of 256 it gets corrupted when loaded by the elf loader
  //To avoid that we create an empty file with size equal to the first integer that is bigger of original file size
  // but also a product of 256
  chunks = file_size / BUF_SIZE;
  if(file_size > chunks*BUF_SIZE){
    chunks++;
  }
  cfs_coffee_reserve(file, chunks*BUF_SIZE);
  fd = cfs_open(file, CFS_READ | CFS_WRITE);
  if(fd < 0) {
    update_struct.status = READ_ERROR; //operational_status_inform that something went wrong with opening the file
    process_post(&main_process, event_update_done, &update_struct);
    PROCESS_END();
  }
  //Just make sure that the file contents are all zeros
  int i;
  for(i = 0; i < chunks; i++) {
    memset(buf, 0 , sizeof(buf));
    if(cfs_write(fd, buf, sizeof(buf)) != sizeof(buf)) {
      cfs_close(fd);
      update_struct.status = WRITE_ERROR;	//operational_status_inform that something went wrong with writing on the file
      process_post(&main_process, event_update_done, &update_struct);
      PROCESS_END();
    }
  }
    //Activate Deluge file transfer protocol. 
    res = deluge_disseminate(file, node_id == SINK_ADDR_0);
    if(res == -1) {	//inform that something went wrong with dissemination
      update_struct.status = DELUGE_ERROR;
      process_post(&main_process, event_update_done, &update_struct);
      PROCESS_END();
    }
    update_struct.status = DELUGE_STARTED;
    //Notify the main process (and the coordinator in turn) that the transfer has started
    process_post(&main_process, event_update_done, &update_struct);
    cfs_close(fd);
    //Deluge protocol process now is running. Wait until we receive a final event form it 
    PROCESS_WAIT_EVENT_UNTIL(ev == deluge_event);
    process_exit(&deluge_process); 
    update_struct.status = UPDATE_SUCCESS;

    leds_toggle(LEDS_BLUE);
    //Copy the received file into boot.ce
    res = copy_file(file, "boot.ce");
    if(res == 0) {	//inform that something went wrong when copying the file
      update_struct.status = COPY_ERROR;
      process_post(&runicast_wait, event_update_done, &update_struct);
      cfs_remove(file);
      PROCESS_END();
    }
    //All file operations finished successfully. It's time to load the ELF so that it starts execution
    res = load_component(file);
    if(res == 0) {	//operational_status_inform that something went wrong with the update
      update_struct.status = LOADER_ERROR;
    }
    //Notify main either for final success or for a loader error
    process_post(&main_process, event_update_done, &update_struct);
  PROCESS_END();
}

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(sensor_process, ev, data)
{
  static struct sensor_values vals;
  static mask_struct masks;
  static uint16_t battery;
  static unsigned long timestamp;
  
  PROCESS_BEGIN();
  
  while(1) {
    //Wait until an event to activate the sensors arrives
    PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_REQ_SENSOR);
    SENSORS_ACTIVATE(battery_sensor);
    battery = battery_sensor.value(0); //Read battery value
    timestamp = (unsigned long)clock_time()/CLOCK_SECOND;
    //Extract sensor and process masks structure from data
    //This values indicate from which sensors to sample and where to send the measurements
    masks = *(struct mask_struct*)data;
    //Measure from selected sensors
    if(masks.std_sensor_mask & 0x03 || masks.app_sensor_mask & 0x03) {
      //By default the hardware activates temp/hum sensors together. Moreover humidity depends on temperature, so event
      //if the user has not selected both sensors, we have to measure anyway.
      SENSORS_ACTIVATE(sht11_sensor);
      leds_toggle(LEDS_GREEN);
      vals.values[TEMP] = temperatureC(sht11_sensor.value(SHT11_SENSOR_TEMP));
      vals.values[HUM] = humidity(vals.values[TEMP], sht11_sensor.value(SHT11_SENSOR_HUMIDITY));
    }
    if(masks.std_sensor_mask & 0x0B || masks.app_sensor_mask & 0x0B) {
      //Same as the previous one
      SENSORS_ACTIVATE(light_sensor);
      vals.values[LIGHT1] = 2.289*light_sensor.value(LIGHT_SENSOR_PHOTOSYNTHETIC);//TODO: Possibly wrong conversion
      vals.values[LIGHT2] = 0.2816*light_sensor.value(LIGHT_SENSOR_TOTAL_SOLAR);
    }
    //target_process_mask value implies to which process the data should be delivered
    //In case of periodic or single queries data will be delivered to the main process
    if(((masks.target_process_mask & PERIODIC_QUERY_PROCESS) && prd_request_on) 
      || (masks.target_process_mask & SINGLE_QUERY_PROCESS)) {
      //Construct a message structure and fill it with the appropriate values.
      //This is a temporary solution. Data structures will be changed so that this code won't be so messy
      struct data_msg_struct msg;
      struct data_container_struct packed_msg;
      float sensor_data[SENSOR_NUM];
      msg.sensor_mask = masks.std_sensor_mask;
      msg.bt = batteryV(battery);
      int i, data_size;
      int j = 0;
      for(i = 0; i < SENSOR_NUM; i++) {
	if(masks.std_sensor_mask & (0x01 << i)) {
	  sensor_data[j] = vals.values[i];
	  j++;
	}
      }
      data_size = j*sizeof(float);
      if(masks.target_process_mask & PERIODIC_QUERY_PROCESS) {
	msg.from = PERIODIC_QUERY_DATA | (version_id << 2);
	msg.timestamp = prd_request_seq_id++;
	msg.timestamp |= ((long)rates.prd_gcd_rate) << 16;
	msg.padding = process_is_running(&deluge_process);
	msg.padding |= cc2420_get_txpower() << 1; 
	if(prd_request_seq_id == 0){ prd_request_seq_id = 1;}
	packed_msg.msg_size = create_packet((void*)sensor_data, &msg, data_size, 0);
	create_packet((void*)(&timestamp), &msg, sizeof(unsigned long), data_size);
	packed_msg.msg_size += sizeof(unsigned long);
      }
      else {
	msg.from = SINGLE_QUERY_DATA | (version_id << 2);
	msg.timestamp = timestamp;
	msg.packet_seq_no = process_is_running(&deluge_process);
	msg.padding = cc2420_get_txpower();
	packed_msg.msg_size = create_packet((void*)sensor_data, &msg, data_size, 0);
      }
      packed_msg.msg = &msg;
      process_post_synch(&main_process, event_runicast_send, &packed_msg);
    }
    //In case the request came from a user application the collected data is delivered to the application process
    if((masks.target_process_mask & APP_QUERY_PROCESS) && app_request_on){
      int i;
      vals.from = USER_APPLICATION_DATA | (version_id << 2);
      vals.sensor_mask = masks.app_sensor_mask;
      vals.timestamp = (unsigned long)clock_time()/CLOCK_SECOND;
      vals.tx_num = tx_num;
      vals.bt = batteryV(battery);
      for(i = 0; elfloader_autostart_processes[i] != NULL; ++i) {
	process_post(elfloader_autostart_processes[i], PROCESS_EVENT_SENSOR_VALUES, &vals);
      }
    }
    //If application or standanrd periodic queries are still valid, then re-enable the timer
    if(rates.gcd_rate > 0) {
      ctimer_set(&ct, CLOCK_SECOND * rates.gcd_rate, timercb, NULL);
    }
    //Deactivate all sensors
    SENSORS_DEACTIVATE(sht11_sensor);
    SENSORS_DEACTIVATE(light_sensor);
    SENSORS_DEACTIVATE(battery_sensor);
  }


  PROCESS_END();
}
/*---------------------------------------------------------------------------*/

