#include "contiki.h"

// Virtual timer struct
typedef struct {
  unsigned int rate;
  unsigned int ref_rate;
  int counter;
} vtimer;

void vtimer_set(vtimer* vt, unsigned int rate, unsigned int ref_rate);

void vtimer_tick(vtimer* vt);

void vtimer_reset(vtimer* vt);

uint8_t vtimer_expired(vtimer* vt);

void vtimer_force(vtimer* vt);

void vtimer_sync(vtimer* vt, struct ctimer* ct, unsigned int new_ref_rate);