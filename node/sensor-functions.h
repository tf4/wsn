#ifndef __SENSOR_FUNCTIONS_H__
#define __SENSOR_FUNCTIONS_H__

float temperatureC(unsigned int rawdata);


float humidity(float tempdata, unsigned int humrawdata);

float batteryV(uint16_t battery);

#endif
